export interface Currency {
  code: string;
  value: number;
}
/*
Models just describe a data structure.
*/
