import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {
  constructor(private httpClient: HttpClient) { }

  public getRates() {
    return this.httpClient.get<any>(`https://api.exchangeratesapi.io/latest?base=USD`);
  }
}

/*
The currency service
That service is quite simple. All we do here is to call the API-Endpoint and return the response as an observable.

This service is generic enough to use for most RESTful apis.
*/


