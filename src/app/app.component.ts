import { Component, OnInit, Input } from '@angular/core';
import { CurrencyService } from './currency.service';
import { Currency } from './models/currency';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Currency Rates';
  currencyRates: Currency[];
  amount = 1;

  constructor(private currencyService: CurrencyService) { }

  updateAmounts(num){
    this.amount = num;
  }

  ngOnInit() {
    this.currencyService.getRates().subscribe( (data) => {
      this.currencyRates = Object.keys(data.rates).map((key) => {
           return {code: key, value: data.rates[key]}
         });
    });
  }
}
